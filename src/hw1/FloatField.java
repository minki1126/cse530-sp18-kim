package hw1;
import java.io.*;
import java.nio.ByteBuffer;
import java.util.Objects;

/**
 * Instance of Field that stores a single float.
 */
public class FloatField implements Field {
	private float value;

	public float getValue() {
		return value;
	}

	/**
	 * Constructor.
	 *
	 * @param f The value of this field.
	 */
	public FloatField(float f) {
		value = f;
	}

	public FloatField(byte[] b) {
		value = java.nio.ByteBuffer.wrap(b).getFloat();
	}

	public String toString() {
		return Float.toString(value);
	}

	public int hashCode() {
		return Objects.hashCode(value);
	}

	public boolean equals(Object field) {
		return ((FloatField) field).value == value;
	}

	public void serialize(DataOutputStream dos) throws IOException {
		dos.writeFloat(value);
	}

	public byte[] toByteArray() {
		return ByteBuffer.allocate(4).putFloat(value).array();
	}

	/**
	 * Compare the specified field to the value of this Field.
	 * Return semantics are as specified by Field.compare
	 *
	 * @throws IllegalCastException if val is not a FloatField
	 * @see Field#compare
	 */
	public boolean compare(RelationalOperator op, Field val) {

		FloatField iVal = (FloatField) val;

		switch (op) {
		case EQ:
			return value == iVal.value;
		case NOTEQ:
			return value != iVal.value;

		case GT:
			return value > iVal.value;

		case GTE:
			return value >= iVal.value;

		case LT:
			return value < iVal.value;

		case LTE:
			return value <= iVal.value;
		}

		return false;
	}

	/**
	 * Return the Type of this field.
	 * @return Type.INT_TYPE
	 */
	public Type getType() {
		return Type.FLOAT;
	}
}

