package hw1;

import java.io.*;
import java.nio.ByteBuffer;
import java.util.Date;

/**
 * Instance of Field that stores the date.
 */
public class DateField implements Field {
    private long secondsValue;
    private Date value;

    public Date getValue() {
        return value;
    }
    public long getSecValue() {
    	return secondsValue;
    }
    /**
     * Constructor.
     *
     * @param i The value of this field.
     */
    public DateField(Date d) {
        value = d;
        secondsValue = d.getTime();
    }
    /**
     * Constructor with seconds value
     *
     * @param i The value of this field.
     */
    public DateField(long s) {
    	secondsValue = s;
    	value = new Date(s);
    }
    
    public DateField(byte[] b) {
    	secondsValue = java.nio.ByteBuffer.wrap(b).getLong();
    	value = new Date(secondsValue);
    }

    public String toString() {
        return value.toString();
    }

    public int hashCode() {
        return value.hashCode();
    }

    public boolean equals(Object field) {
        return ((DateField) field).value == value;
    }

    public void serialize(DataOutputStream dos) throws IOException {
    	dos.writeLong(secondsValue);
    }
    
    public byte[] toByteArray() {
    	return ByteBuffer.allocate(8).putLong(secondsValue).array();
    }

    /**
     * Compare the specified field to the value of this Field.
     * Return semantics are as specified by Field.compare
     *
     * @throws IllegalCastException if val is not a DateField
     * @see Field#compare
     */
    public boolean compare(RelationalOperator op, Field val) {

        DateField iVal = (DateField) val;

        switch (op) {
        case EQ:
            return value == iVal.value;
        case NOTEQ:
            return value != iVal.value;

        case GT:
            return secondsValue > iVal.secondsValue;

        case GTE:
            return secondsValue >= iVal.secondsValue;

        case LT:
            return secondsValue < iVal.secondsValue;

        case LTE:
            return secondsValue <= iVal.secondsValue;
        }

        return false;
    }

    /**
     * Return the Type of this field.
     * @return Type.INT_TYPE
     */
	public Type getType() {
		return Type.DATE;
	}
}
