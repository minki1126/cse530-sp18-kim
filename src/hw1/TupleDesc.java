package hw1;
import java.util.*;

/**
 * TupleDesc describes the schema of a tuple.
 */
public class TupleDesc {

	private Type[] types;
	private String[] fields;
	private final int size;
	private static final int SIZEOFINT = 4;
	private static final int SIZEOFSTRING = 129;
	private static final int SIZEOFFLOAT = 4;
	private static final int SIZEOFDATE = 32;
    /**
     * Create a new TupleDesc with typeAr.length fields with fields of the
     * specified types, with associated named fields.
     *
     * @param typeAr array specifying the number of and types of fields in
     *        this TupleDesc. It must contain at least one entry.
     * @param fieldAr array specifying the names of the fields. Note that names may be null.
     */
    public TupleDesc(Type[] typeAr, String[] fieldAr) {
    	this.types = typeAr;
    	this.fields = fieldAr;
    	this.size = this.calculateSize();
    }

    public Type[] getTypeArr(){
    	Type [] copy = new Type[this.types.length];
    	System.arraycopy(this.types, 0, copy, 0, this.types.length);
    	return copy;
    }
    
    public String [] getFieldNameArr() {
    	String [] copy = new String[this.fields.length];
    	System.arraycopy(this.fields, 0, copy, 0, this.fields.length);
    	return copy;
    }
    /**
     * @return the number of fields in this TupleDesc
     */
    public int numFields() {
    	return this.fields.length;
    }

    /**
     * Gets the (possibly null) field name of the ith field of this TupleDesc.
     *
     * @param i index of the field name to return. It must be a valid index.
     * @return the name of the ith field
     * @throws NoSuchElementException if i is not a valid field reference.
     */
    public String getFieldName(int i) throws NoSuchElementException {
    	if (i < 0 || i >= fields.length){
    		throw new NoSuchElementException();
    	}
    	return fields[i];
    }

    /**
     * Find the index of the field with a given name.
     *
     * @param name name of the field.
     * @return the index of the field that is first to have the given name.
     * @throws NoSuchElementException if no field with a matching name is found.
     */
    public int nameToId(String name) throws NoSuchElementException {
    	for (int index = 0; index < fields.length; ++index){
    		if (name.equals(this.fields[index])){
    			return index;
    		}
    	}
 
    	throw new NoSuchElementException();
    }

    /**
     * Gets the type of the ith field of this TupleDesc.
     *
     * @param i The index of the field to get the type of. It must be a valid index.
     * @return the type of the ith field
     * @throws NoSuchElementException if i is not a valid field reference.
     */
    public Type getType(int i) throws NoSuchElementException {
    	if (i < 0 || i >= fields.length){
    		throw new NoSuchElementException();
    	}
    	return types[i];
    }

    /**
     * @return The size (in bytes) of tuples corresponding to this TupleDesc.
     * Note that tuples from a given TupleDesc are of a fixed size.
     */
    public int getSize() {
    	return this.size;
    }
   
    /*
     * Calculates the size (in bytes) of the tuple being described
     * */
    private int calculateSize(){
    	int temp = 0;
    	for (int i = 0; i < this.types.length; ++i){
    		switch(this.types[i]){
    			case INT: 
    				temp += TupleDesc.SIZEOFINT;
    				break;
    			case STRING: 
    				temp += TupleDesc.SIZEOFSTRING;
			 	    break;	 
    			case FLOAT:
    				temp += TupleDesc.SIZEOFFLOAT;
    				break;
    			case DATE:
    				temp += TupleDesc.SIZEOFDATE;
    				break;
			 	default:
			 		break;
    		}
    	}
    	return temp;
    }

    /**
     * Compares the specified object with this TupleDesc for equality.
     * Two TupleDescs are considered equal if they are the same size and if the
     * n-th type in this TupleDesc is equal to the n-th type in td.
     *
     * @param o the Object to be compared for equality with this TupleDesc.
     * @return true if the object is equal to this TupleDesc.
     */
    public boolean equals(Object o) {
    	/*https://www.geeksforgeeks.org/overriding-equals-method-in-java/
    	 * I referenced this source for the self comparing lines, and for the use of
    	 * instanceof keyword
    	 * */
	
    	if (o == this){
    		return true;
    	}
    	
    	if (!(o instanceof TupleDesc)) {
            return false;
        }
    	TupleDesc td = (TupleDesc) o;
    	
    	if (this.size != td.getSize() || this.numFields() != td.numFields()){
    		return false;
    	}
    	
    	for (int i = 0; i < this.types.length; ++i){
    		if (this.getType(i) != td.getType(i)){
    			return false;
    		}
    	}
    	
    	return true;
    }
    

    public int hashCode() {
        //https://stackoverflow.com/questions/113511/best-implementation-for-hashcode-method
    	int result = 23;
    	int c = this.getSize(); 
    	result = 37 * result + c;
    	c = this.numFields();
    	result = 37 * result + c;
        return result;
    }

    /**
     * Returns a String describing this descriptor. It should be of the form
     * "fieldType[0](fieldName[0]), ..., fieldType[M](fieldName[M])", although
     * the exact format does not matter.
     * @return String describing this descriptor.
     */
    public String toString() {
    	StringBuffer result = new StringBuffer();
        for (int i = 0; i < this.fields.length; ++i){
        	String type="";
        	switch(this.types[i]){
			case INT: 
				type = "INT";
				break;
			case STRING: 
				type = "STRING";
		 	    break;	
			case FLOAT:
				type = "FLOAT";
				break;
			case DATE:
				type = "DATE";
				break;
		 	default:
		 		break;
		}
        result.append(type);
        result.append("(");
        result.append(fields[i] != null ? fields[i] : "NULL");
        result.append(i == this.fields.length - 1 ? ")" : "), ");
        }
    	return result.toString();
    }
}
