package hw1;

import java.sql.Types;
import java.nio.ByteBuffer;
import java.util.HashMap;

/**
 * This class represents a tuple that will contain a single row's worth of information
 * from a table. It also includes information about where it is stored
 * @author Sam Madden modified by Doug Shook
 *
 */
public class Tuple {
	private TupleDesc tupleDescriptor;
	private Field[] fields;
	private int pid;
	private int slotId;
	/**
	 * Creates a new tuple with the given description
	 * @param t the schema for this tuple
	 */
	public Tuple(TupleDesc t) {
		this.setDesc(t);
		this.fields = new Field[t.numFields()];
	}
	
	public TupleDesc getDesc() {
		return this.tupleDescriptor;
	}
	
	/**
	 * retrieves the page id where this tuple is stored
	 * @return the page id of this tuple
	 */
	public int getPid() {
		return this.pid;
	}

	public void setPid(int pid) {
		this.pid = pid;
	}

	/**
	 * retrieves the tuple (slot) id of this tuple
	 * @return the slot where this tuple is stored
	 */
	public int getId() {
		return this.slotId;
	}

	public void setId(int id) {
		this.slotId = id;
	}
	
	public void setDesc(TupleDesc td) {
		this.tupleDescriptor = td;
	}
	
	/**
	 * Stores the given data at the i-th field
	 * @param i the field number to store the data
	 * @param v the data
	 */
	public void setField(int i, Field v) {
		if (i < 0 || i >= this.fields.length){
			return;
		}
		this.fields[i] = v;
	}
	
	public Field getField(int i) {
		return (i < 0 || i >= this.fields.length) ? null : this.fields[i]; 
	}
	
	/**
	 * Creates a string representation of this tuple that displays its contents.
	 * You should convert the binary data into a readable format (i.e. display the ints in base-10 and convert
	 * the String columns to readable text).
	 */
	public String toString() {
    	StringBuffer result = new StringBuffer();
    	for (int i = 0; i < this.fields.length; ++i){
        	String temp="";
        	if (this.fields[i] == null){continue;}
        	switch(this.fields[i].getType()){
			case INT: 
				result.append(this.fields[i].toString());
				break;
			case STRING: 
				result.append(this.fields[i].toString());
		 	    break;
			case FLOAT:
				result.append(this.fields[i].toString());
				break;
			case DATE:
				result.append(this.fields[i].toString());
				break;
		 	default:
		 		break;
		}
        result.append(i == this.fields.length - 1 ? "" : ", ");
        }
		return result.toString();
	}
}
	