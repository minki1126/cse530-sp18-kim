package hw1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * The Catalog keeps track of all available tables in the database and their
 * associated schemas.
 * For now, this is a stub catalog that must be populated with tables by a
 * user program before it can be used -- eventually, this should be converted
 * to a catalog that reads a catalog table from disk.
 */

public class Catalog {
	public Map<String,Table> tableMapByName;
	public Map<Integer,Table> tableMapById;
    /**
     * Constructor.
     * Creates a new, empty catalog.
     */
    public Catalog() {
    	this.tableMapByName = new HashMap<String,Table>();
    	this.tableMapById = new HashMap<Integer,Table>();

    }

    private class Table {
    	public HeapFile file;
    	public String name;
    	public String primaryKeyName;
    	
    	/*Page Lock Table*/
    	public HashMap<Integer, HashSet<Integer>> transReadLockTable;
    	public HashMap<Integer, HashSet<Integer>> transWriteLockTable;
    	/*Inverse Mappings*/
    	public HashMap<Integer, HashSet<Integer>> pageTransReadLockTable;
    	public HashMap<Integer, HashSet<Integer>> pageTransWriteLockTable;
    	
    	public Table(HeapFile file, String name, String pkeyField){
    		this.file = file;
    		this.name = name;
    		this.primaryKeyName = pkeyField;
    		this.transReadLockTable = new HashMap<>();
    		this.transWriteLockTable = new HashMap<>();
    		this.pageTransReadLockTable = new HashMap<>();
    		this.pageTransWriteLockTable = new HashMap<>();
    	}
    	
    	public boolean transHasReadLock (int tid, int heapPageId){
    		HashSet<Integer> readLocks = this.transReadLockTable.get(tid);
    		if (readLocks != null){
    			return readLocks.contains(heapPageId);
    		}
     		return false;
    	}
    	
    	public boolean transHasWriteLock (int tid, int heapPageId){
    		HashSet<Integer> writeLocks = this.transWriteLockTable.get(tid);
    		if (writeLocks != null){
    			return writeLocks.contains(heapPageId);
    		}
     		return false;
    	}
    	
    	public boolean readLockOnPage(int heapPageId){
    		HashSet<Integer> readers = this.pageTransReadLockTable.get(heapPageId);
    		if (readers != null){
    			return readers.size() > 0;
    		}
    		return false;
    	}
    	
    	public boolean isLoneReader(int tid, int heapPageId){
    		HashSet<Integer> readers = this.pageTransReadLockTable.get(heapPageId);
    		if (readers != null){
    			return readers.size() == 1 && readers.contains(tid);
    		}
    		return false;
    	}
    	
    	public boolean writeLockOnPage(int heapPageId){
    		HashSet<Integer> writers = this.pageTransWriteLockTable.get(heapPageId);
    		if (writers != null){
    			//assert writers.size() <= 1; /*For debugging*/
    			return writers.size() > 0;
    		}
    		return false;
    	}
    	
    	/*Link both mapping directions*/
    	public void addReadLock(int tid, int heapPageId){
    		this.transReadLockTable.compute(tid, (k, v) ->{
    			if (v == null){
    				HashSet<Integer> pageList = new HashSet<>();
    				pageList.add(heapPageId);
    				return pageList;
    			}
    			v.add(heapPageId);
    			return v;
    		});
    		this.pageTransReadLockTable.compute(heapPageId, (k, v) -> {
    			if (v == null){
    				HashSet<Integer> readers = new HashSet<>();
    				readers.add(tid);
    				return readers;
    			}
    			v.add(tid);
    			return v;
    		});
    	}
    	
    	/*Link both mapping directions*/
    	public void addWriteLock(int tid, int heapPageId){
    		this.transWriteLockTable.compute(tid, (k, v) ->{
    			if (v == null){
    				HashSet<Integer> pageList = new HashSet<>();
    				pageList.add(heapPageId);
    				return pageList;
    			}
    			v.add(heapPageId);
    			return v;
    		});
    		
    		this.pageTransWriteLockTable.compute(heapPageId, (k, v) -> {
    			if (v == null){
    				HashSet<Integer> writers = new HashSet<>();
    				writers.add(tid);
    				return writers;
    			}
    			//assert v.isEmpty(); /*Two writers have acquired the lock - illegal*/
    			v.add(tid);
    			return v;
    		});
    	
    	}
    	
    	public void removeWriteLock(int tid, int heapPageId){
    		this.transWriteLockTable.compute(tid, (k, v) -> {
    			if (v == null){
    				return v;
    			}
    			v.remove(heapPageId);
    			return v;
    		});
    		this.pageTransWriteLockTable.compute(heapPageId, (k, v) -> {
    			if (v == null){
    				return v;
    			}
    			v.remove(tid);
    			return v;
    		});
    	}
    	
    	public void removeReadLock(int tid, int heapPageId){
    		this.transReadLockTable.compute(tid, (k, v) -> {
    			if (v == null){
    				return v;
    			}
    			v.remove(heapPageId);
    			return v;
    		});
    		this.pageTransReadLockTable.compute(heapPageId, (k, v) -> {
    			if (v == null){
    				return v;
    			}
    			v.remove(tid);
    			return v;
    		});
    	}
    	
    	public void releaseAllLocks(int tid){
    		HashSet<Integer> readLockedPages = this.transReadLockTable.remove(tid);
    		HashSet<Integer> writeLockedPages = this.transWriteLockTable.remove(tid);
    		if (readLockedPages != null){
    			readLockedPages.forEach(pid -> this.pageTransReadLockTable.get(pid).remove(tid));
    		}
    		if (writeLockedPages != null){
    			writeLockedPages.forEach(pid -> this.pageTransWriteLockTable.get(pid).remove(tid));
    		}
    	}
    	
    	public HashSet<Integer> getWriteLockedPages(int tid){
    		return this.transWriteLockTable.get(tid);
     	}
    }
    
    /**
     * Add a new table to the catalog.
     * This table's contents are stored in the specified HeapFile.
     * @param file the contents of the table to add;  file.getId() is the identfier of
     *    this file/tupledesc param for the calls getTupleDesc and getFile
     * @param name the name of the table -- may be an empty string.  May not be null.  If a name conflict exists, use the last table to be added as the table for a given name.
     * @param pkeyField the name of the primary key field
     */
    public void addTable(HeapFile file, String name, String pkeyField) {
    	Table t = new Table(file, name, pkeyField);
    	
    	/*Insert new Table and in the case of name collision get old one back*/
    	Table old_t = this.tableMapByName.put(name, t);
    	
    	/*Remove the table that was previously associated with name*/
    	if (old_t != null){
    		int t_id = old_t.file.getId();
    		this.tableMapById.remove(t_id);
    	}
    	
    	/*Insert the new table associated with name*/
    	this.tableMapById.compute(file.getId(),(k,v) -> t);
    }

    public void addTable(HeapFile file, String name) {
        addTable(file,name,"");
    }

    /**
     * Return the id of the table with a specified name,
     * @throws NoSuchElementException if the table doesn't exist
     */
    public int getTableId(String name) {
    	Table t = this.tableMapByName.get(name);
    	
    	if (t == null){
    		throw new NoSuchElementException();
    	}

    	return t.file.getId();
    }
    
    public HeapPage getPageFromTable(int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		HeapFile f = t.file;
    		if (f != null){
    			return f.readPage(pid);
    		}
    	}
    	return null;
    }

    /**
     * Returns the tuple descriptor (schema) of the specified table
     * @param tableid The id of the table, as specified by the DbFile.getId()
     *     function passed to addTable
     */
    public TupleDesc getTupleDesc(int tableid) throws NoSuchElementException {
    	Table t = this.tableMapById.get(tableid);
    	
    	if (t == null){
    		throw new NoSuchElementException();
    	}
    	
    	return t.file.getTupleDesc();
    }

    public boolean holdsLock(int tid, int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		return t.transHasReadLock(tid, pid) || t.transHasWriteLock(tid, pid);
    	}
    	return false;
    }
    
    public boolean holdsReadLock(int tid, int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		return t.transHasReadLock(tid, pid);
    	}
    	return false;
    }
    
    public boolean holdsWriteLock(int tid, int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		return t.transHasWriteLock(tid, pid);
    	}
    	return false;
    }
   
    public boolean pageIsReadLocked(int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		return t.readLockOnPage(pid);
    	}
    	return false;
    }
    public boolean pageIsWriteLocked(int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		return t.writeLockOnPage(pid);
    	}
    	return false;
    }
    
    public boolean pageIsLocked(int tableId, int pid){
    	return this.pageIsReadLocked(tableId, pid) || this.pageIsWriteLocked(tableId, pid);
    }
    
    public boolean tryReadLock(int tid, int tableId, int pid){
    	/*If we already have a read or a write lock we can just return*/
    	if (this.holdsLock(tid, tableId, pid)){
    		return true;
    	}
  
    	/*Bail if the page is write locked by someone else*/
    	if (this.pageIsWriteLocked(tableId, pid)){
    		return false;
    	}
    	
    	/*No writers, we don't have lock - we can obtain it*/
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		t.addReadLock(tid, pid);
    		return true; /*success*/
    	}
    	return false;
    }
    
    public boolean tryWriteLock(int tid, int tableId, int pid){
    	/*If we already have W Lock - bail*/
    	if (this.holdsWriteLock(tid, tableId, pid)){
    		return true;
    	}
    	
    	/*Someone else has a W Lock - bail*/
    	if (this.pageIsWriteLocked(tableId, pid)){
    		return false;
    	}
    	
    	Table t = this.tableMapById.get(tableId);
    	if (t == null){
    		return false;
    	}
    	
       	/*No writers, check for readers*/
    	if (this.pageIsReadLocked(tableId, pid)){
    		/*try to promote*/
    		if (this.holdsReadLock(tid, tableId, pid) && t.isLoneReader(tid, pid)){
    			t.removeReadLock(tid, pid);
    			t.addWriteLock(tid, pid);
    			return true;
    		}
    		else{
    			return false; /*bail*/
    		}
    	}
    	
    	/*Page is not R Locked or W Locked*/
    	//assert this.pageIsReadLocked(tableId, pid) == false && this.pageIsWriteLocked(tableId, pid) == false;
    	
    	t.addWriteLock(tid, pid);
    	return true;
    }
    
    public void releaseLock(int tid, int tableId, int pid){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		if (this.holdsReadLock(tid, tableId, pid)){
    			t.removeReadLock(tid, pid);
    		}else if (this.holdsWriteLock(tid, tableId, pid)){
    			t.removeWriteLock(tid, pid);
    		}
    	}
    }
    
    public HashSet<Integer> getWriteLockedPages(int tid, int tableId){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		return t.getWriteLockedPages(tid);
    	}
    	return null;
    }
    
    public void releaseAllLocks(int tid, int tableId){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		t.releaseAllLocks(tid);
    	}
    }
    
    public void writeHeapPage(int tableId, HeapPage hp){
    	Table t = this.tableMapById.get(tableId);
    	if (t != null){
    		t.file.writePage(hp);
    	}
    }
    
    /**
     * Returns the HeapFile that can be used to read the contents of the
     * specified table.
     * @param tableid The id of the table, as specified by the HeapFile.getId()
     *     function passed to addTable
     */
    public HeapFile getDbFile(int tableid) throws NoSuchElementException {
    	Table t = this.tableMapById.get(tableid);
    	
    	if (t == null){
    		throw new NoSuchElementException();
    	}
    	
    	return t.file;
    }

    /** Delete all tables from the catalog */
    public void clear() {
    	this.tableMapById.clear();
    	this.tableMapByName.clear();
    }

    public String getPrimaryKey(int tableid) {
    	Table t = this.tableMapById.get(tableid);
    	
    	if (t == null){
    		throw new NoSuchElementException();
    	}
    	
    	return t.primaryKeyName;
    }

    public Iterator<Integer> tableIdIterator() {
    	return this.tableMapById.keySet().iterator();
    }

    public String getTableName(int id) {
    	Table t = this.tableMapById.get(id);
    	
    	if (t == null){
    		throw new NoSuchElementException();
    	}
    	
    	return t.name;
    }
    
    /**
     * Reads the schema from a file and creates the appropriate tables in the database.
     * @param catalogFile
     */
    public void loadSchema(String catalogFile) {
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File(catalogFile)));

            while ((line = br.readLine()) != null) {
                //assume line is of the format name (field type, field type, ...)
                String name = line.substring(0, line.indexOf("(")).trim();
                //System.out.println("TABLE NAME: " + name);
                String fields = line.substring(line.indexOf("(") + 1, line.indexOf(")")).trim();
                String[] els = fields.split(",");
                ArrayList<String> names = new ArrayList<String>();
                ArrayList<Type> types = new ArrayList<Type>();
                String primaryKey = "";
                for (String e : els) {
                    String[] els2 = e.trim().split(" ");
                    names.add(els2[0].trim());
                    if (els2[1].trim().toLowerCase().equals("int"))
                        types.add(Type.INT);
                    else if (els2[1].trim().toLowerCase().equals("string"))
                        types.add(Type.STRING);
                    else if (els2[1].trim().toLowerCase().equals("float"))
                        types.add(Type.FLOAT);
                    else if (els2[1].trim().toLowerCase().equals("date"))
                        types.add(Type.DATE);
                    else {
                        System.out.println("Unknown type " + els2[1]);
                        System.exit(0);
                    }
                    if (els2.length == 3) {
                        if (els2[2].trim().equals("pk"))
                            primaryKey = els2[0].trim();
                        else {
                            System.out.println("Unknown annotation " + els2[2]);
                            System.exit(0);
                        }
                    }
                }
                Type[] typeAr = types.toArray(new Type[0]);
                String[] namesAr = names.toArray(new String[0]);
                TupleDesc t = new TupleDesc(typeAr, namesAr);
                HeapFile tabHf = new HeapFile(new File("testfiles/" + name + ".dat"), t);
                addTable(tabHf,name,primaryKey);
                System.out.println("Added table : " + name + " with schema " + t);
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(0);
        } catch (IndexOutOfBoundsException e) {
            System.out.println ("Invalid catalog entry : " + line);
            System.exit(0);
        }
    }
}

