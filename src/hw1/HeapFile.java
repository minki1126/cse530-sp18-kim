package hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Iterator;

/**
 * A heap file stores a collection of tuples. It is also responsible for managing pages.
 * It needs to be able to manage page creation as well as correctly manipulating pages
 * when tuples are added or deleted.
 * @author Sam Madden modified by Doug Shook
 *
 */
public class HeapFile {
	
	public static final int PAGE_SIZE = 4096;
	
	private File file;
	private TupleDesc tupleDescriptor;
	
	/**
	 * Creates a new heap file in the given location that can accept tuples of the given type
	 * @param f location of the heap file
	 * @param types type of tuples contained in the file
	 */
	public HeapFile(File f, TupleDesc type) {
		this.file = f;
		this.tupleDescriptor = type;
	}
	
	public File getFile() {
		return this.file;
	}
	
	public TupleDesc getTupleDesc() {
		return this.tupleDescriptor;
	}
	
	/**
	 * Creates a HeapPage object representing the page at the given page number.
	 * Because it will be necessary to arbitrarily move around the file, a RandomAccessFile object
	 * should be used here.
	 * @param id the page number to be retrieved
	 * @return a HeapPage at the given page number
	 * @throws IOException 
	 */
	public HeapPage readPage(int id) {
		long start = id * HeapFile.PAGE_SIZE;
		RandomAccessFile rf;
		try {
			rf = new RandomAccessFile(this.getFile(), "r");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
		byte[] b = new byte[HeapFile.PAGE_SIZE];
		try {
			rf.seek(start);
			rf.readFully(b, 0, HeapFile.PAGE_SIZE);
			rf.close();
			return new HeapPage(id, b, this.getId());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}

	}
	
	/**
	 * Returns a unique id number for this heap file. Consider using
	 * the hash of the File itself.
	 * @return
	 */
	public int getId() {
		return this.file.hashCode();
	}
	
	/**
	 * Writes the given HeapPage to disk. Because of the need to seek through the file,
	 * a RandomAccessFile object should be used in this method.
	 * @param p the page to write to disk
	 * @throws IOException 
	 */
	public void writePage(HeapPage p) {
		RandomAccessFile rf;
		try {
			rf = new RandomAccessFile(this.getFile(), "rw");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return;
		}
		int offset = p.getId() * HeapFile.PAGE_SIZE;
		try {
			long len = rf.length();
			if (offset >= len){
				long newLength = offset + HeapFile.PAGE_SIZE;
				rf.setLength(newLength);
			}
		} catch (IOException e1) {
		}
		byte[] data = p.getPageData();
		try {
			rf.seek(offset);
			rf.write(data, 0, HeapFile.PAGE_SIZE);
			rf.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds a tuple. This method must first find a page with an open slot, creating a new page
	 * if all others are full. It then passes the tuple to this page to be stored. It then writes
	 * the page to disk (see writePage)
	 * @param t The tuple to be stored
	 * @return The HeapPage that contains the tuple
	 * @throws Exception 
	 */
	public HeapPage addTuple(Tuple t) throws Exception {
		for (int i = 0; i < this.getNumPages(); ++i){
			HeapPage p = this.readPage(i);
			if (p.getNextFreeSlot() != -1){
				p.addTuple(t);
				this.writePage(p);
				return p;
			}
		}
		HeapPage p;
		try {
			p = new HeapPage(this.getNumPages(), new byte[HeapFile.PAGE_SIZE], this.getId());
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		p.addTuple(t);
		this.writePage(p);
		return p;
	}
	
	/**
	 * This method will examine the tuple to find out where it is stored, then delete it
	 * from the proper HeapPage. It then writes the modified page to disk.
	 * @param t the Tuple to be deleted
	 * @throws Exception 
	 */
	public void deleteTuple(Tuple t) throws Exception {
		int pageId = t.getPid();
		HeapPage p = this.readPage(pageId);
		p.deleteTuple(t);
		this.writePage(p);
	}
	
	/**
	 * Returns an ArrayList containing all of the tuples in this HeapFile. It must
	 * access each HeapPage to do this (see iterator() in HeapPage)
	 * @return
	 * @throws IOException 
	 */
	public ArrayList<Tuple> getAllTuples() {
		ArrayList<Tuple> list = new ArrayList<>();
		for (int i = 0; i < this.getNumPages(); ++i){
			HeapPage p = this.readPage(i);
			Iterator<Tuple> it = p.iterator();
			it.forEachRemaining(t -> list.add(t));
		}
		return list;
	}
	
	/**
	 * Computes and returns the total number of pages contained in this HeapFile
	 * @return the number of pages
	 */
	public int getNumPages() {
		return (int) (this.file.length() / HeapFile.PAGE_SIZE);
	}
}
