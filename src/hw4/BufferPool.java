package hw4;

import java.io.*;
import java.util.*;

import hw1.Database;
import hw1.HeapPage;
import hw1.Tuple;

/**
 * BufferPool manages the reading and writing of pages into memory from
 * disk. Access methods call into it to retrieve pages, and it fetches
 * pages from the appropriate location.
 * <p>
 * The BufferPool is also responsible for locking;  when a transaction fetches
 * a page, BufferPool which check that the transaction has the appropriate
 * locks to read/write the page.
 */
public class BufferPool {
    /** Bytes per page, including header. */
    public static final int PAGE_SIZE = 4096;

    /** Default number of pages passed to the constructor. This is used by
    other classes. BufferPool should use the numPages argument to the
    constructor instead. */
    public static final int DEFAULT_PAGES = 50;

    /*Fields*/
    private int numPages;
    private HashMap<Integer, HashSet<Integer>> transactionToTableMap;
    private HashMap<Map.Entry<Integer, Integer>, HeapPage> pageCache;
    private HashMap<Integer, HashSet<Map.Entry<Integer,Integer>>> transToDirtyPageMap;
    
    /**
     * Creates a BufferPool that caches up to numPages pages.
     *
     * @param numPages maximum number of pages in this buffer pool.
     */
    public BufferPool(int numPages) {
        this.numPages = numPages;
        this.pageCache = new HashMap<>();
        this.transactionToTableMap = new HashMap<>();
        this.transToDirtyPageMap = new HashMap<>();
    }

    public synchronized int numCachedPages(){
    	return this.pageCache.size();
    }
    
    public synchronized boolean pageCacheFull(){
    	return this.numCachedPages() >= this.numPages;
    }
    
    public synchronized void addAndProcessTransaction(Transaction t) {
    	for (Map.Entry<Map.Entry<Integer, Integer>, Operations> e : t.getTableIdAndPidToOps()) {
    		Operations thisOp = e.getValue();
    		Map.Entry<Integer, Integer> tableIdAndPid = e.getKey();
    		//the key of tableIdAndPid is the table Id, while the value of the tableIdAndPid is the page id 
    		if (thisOp.getType().equals(OperationType.READ)) {
    			try {
					this.getPage(t.getTid(), tableIdAndPid.getKey(), tableIdAndPid.getValue(), Permissions.READ_ONLY);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    		else if(thisOp.getType().equals(OperationType.INSERT)) {
    			try {
					this.getPage(t.getTid(), tableIdAndPid.getKey(), tableIdAndPid.getValue(), Permissions.READ_WRITE);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    		else {
    			try {
					this.getPage(t.getTid(), tableIdAndPid.getKey(), tableIdAndPid.getValue(), Permissions.READ_WRITE);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    	}
    	for (Map.Entry<Map.Entry<Integer, Integer>, Operations> e : t.getTableIdAndPidToOps()) {
    		Operations thisOp = e.getValue();
    		Map.Entry<Integer, Integer> tableIdAndPid = e.getKey();
    		//the key of tableIdAndPid is the table Id, while the value of the tableIdAndPid is the page id 
    		if (thisOp.getType().equals(OperationType.READ)) {
    			break;
    		}
    		else if(thisOp.getType().equals(OperationType.INSERT)) {
    			try {
					this.insertTuple(t.getTid(), tableIdAndPid.getKey(), thisOp.getTuple());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    		else {
    			try {
					this.deleteTuple(t.getTid(), tableIdAndPid.getKey(), thisOp.getTuple());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
    		}
    	}
    }
    /**
     * Retrieve the specified page with the associated permissions.
     * Will acquire a lock and may block if that lock is held by another
     * transaction.
     * <p>
     * The retrieved page should be looked up in the buffer pool.  If it
     * is present, it should be returned.  If it is not present, it should
     * be added to the buffer pool and returned.  If there is insufficient
     * space in the buffer pool, a page should be evicted and the new page
     * should be added in its place.
     *
     * @param tid the ID of the transaction requesting the page
     * @param tableId the ID of the table with the requested page
     * @param pid the ID of the requested page
     * @param perm the requested permissions on the page
     */
    public synchronized HeapPage getPage(int tid, int tableId, int pid, Permissions perm)
        throws Exception {
    	Map.Entry<Integer, Integer> key = new AbstractMap.SimpleEntry<>(tableId, pid);
    	HeapPage hp = this.pageCache.get(key);
    	if (hp == null){
    		/*Page not in cache*/
    		if (this.pageCacheFull()){
    			this.evictPage();
    		}
    		hp = Database.getCatalog().getPageFromTable(tableId, pid);
    		if (hp == null){
    			return null;
    		}
    		/*cache the page*/
    		this.pageCache.put(key, hp);
    	}
    	
        if (perm.permLevel == 0){
        	/*read only*/
        	if (this.tryReadLock(tid, tableId, pid)){
        		return hp;
        	}
        	
        }else if (perm.permLevel == 1){
        	/*write*/
        	if (this.tryWriteLock(tid, tableId, pid)){
        		return hp;
        	}
        	
        }
        
        //Couldn't acquire lock so lets abort
        this.transactionComplete(tid, false);
        return null;
    }

    /**
     * Releases the lock on a page.
     * Calling this is very risky, and may result in wrong behavior. Think hard
     * about who needs to call this and why, and why they can run the risk of
     * calling it.
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param tableID the ID of the table containing the page to unlock
     * @param pid the ID of the page to unlock
     */
    public synchronized void releasePage(int tid, int tableId, int pid) {
        Database.getCatalog().releaseLock(tid, tableId, pid);
    }

    /** Return true if the specified transaction has a lock on the specified page */
    public synchronized  boolean holdsLock(int tid, int tableId, int pid) {
        return Database.getCatalog().holdsLock(tid, tableId, pid);
    }
    
    public synchronized  boolean holdsReadLock(int tid, int tableId, int pid) {
        return Database.getCatalog().holdsReadLock(tid, tableId, pid);
    }
    public synchronized  boolean holdsWriteLock(int tid, int tableId, int pid) {
        return Database.getCatalog().holdsWriteLock(tid, tableId, pid);
    }
    
    public synchronized boolean tryReadLock(int tid, int tableId, int pid){
    	if (Database.getCatalog().tryReadLock(tid, tableId, pid)){
    		this.transactionToTableMap.compute(tid, (k, v) -> {
    			if (v == null){
    				HashSet<Integer> tableIds = new HashSet<>();
    				tableIds.add(tableId);
    				return tableIds;
    			}
    			v.add(tableId);
    			return v;
    		});
    		return true;
    	}
    	return false;
    }
    
    public synchronized boolean tryWriteLock(int tid, int tableId, int pid){
    	if (Database.getCatalog().tryWriteLock(tid, tableId, pid)){
    		this.transactionToTableMap.compute(tid, (k, v) -> {
    			if (v == null){
    				HashSet<Integer> tableIds = new HashSet<>();
    				tableIds.add(tableId);
    				return tableIds;
    			}
    			v.add(tableId);
    			return v;
    		});
    		return true;
    	}
    	return false;
    }
    
    public  void releaseAllLocks(int tid){
    	HashSet<Integer> tableIds = this.transactionToTableMap.get(tid);
    	if (tableIds != null){
    		tableIds.forEach(tableId -> Database.getCatalog().releaseAllLocks(tid, tableId));
    	}
    }
   
    /**
     * Commit or abort a given transaction; release all locks associated to
     * the transaction. If the transaction wishes to commit, write
     *
     * @param tid the ID of the transaction requesting the unlock
     * @param commit a flag indicating whether we should commit or abort
     */
    public synchronized  void transactionComplete(int tid, boolean commit)
        throws IOException {
    	HashSet<Map.Entry<Integer, Integer>> dirtyPages = this.transToDirtyPageMap.get(tid);
    	dirtyPages = dirtyPages != null ? dirtyPages : new HashSet<>();
        if (commit){
        	/*Write Back all Pages to disk*/
        	for (Map.Entry<Integer, Integer> page : dirtyPages){
        		this.flushPage(page.getKey(), page.getValue());
        	}
        }
        else{
        	/*Replace all Dirty Pages with clean versions from disk*/
        	for (Map.Entry<Integer, Integer> page : dirtyPages){
        		HeapPage hp = Database.getCatalog().getPageFromTable(page.getKey(), page.getValue());
        		this.pageCache.put(page, hp);
        	}
        }
        /*Cleanup:
         * 1) Realease all Locks
         * 2) Remove tid from dirty page map
         * 3) Remove tid from table map*/
        this.releaseAllLocks(tid);
        this.transToDirtyPageMap.remove(tid);
        this.transactionToTableMap.remove(tid);
     
    }

    /**
     * Add a tuple to the specified table behalf of transaction tid.  Will
     * acquire a write lock on the page the tuple is added to. May block if the lock cannot 
     * be acquired.
     * 
     * Marks any pages that were dirtied by the operation as dirty
     *
     * @param tid the transaction adding the tuple
     * @param tableId the table to add the tuple to
     * @param t the tuple to add
     */
    public synchronized void insertTuple(int tid, int tableId, Tuple t)
        throws Exception {
    	HashSet<Integer> candidatePages = Database.getCatalog().getWriteLockedPages(tid, tableId);
    	candidatePages = candidatePages != null ? candidatePages : new HashSet<Integer>();
    	HeapPage hp = null;
    	int p = 0;
    	for (Integer pid : candidatePages){
    		Map.Entry<Integer,Integer> key = new AbstractMap.SimpleEntry<>(tableId, pid);
    		HeapPage temp = this.pageCache.get(key);
    		//assert temp != null; // page should be in cache
    		if (temp.getNextFreeSlot() != -1){
    			hp = temp;
    			p = pid;
    			break;
    		}
    	}
    	
    	if (hp == null){
    		//none of locked pages had free space...
    		return;
    	}
    	
    	hp.addTuple(t);
    	hp.dirty = true;
    	
    	/*Associate Dirty page with Transaction*/
    	Map.Entry<Integer, Integer> pageEntry = new AbstractMap.SimpleEntry<>(tableId, p);
    	this.transToDirtyPageMap.compute(tid, (k , v) -> {
    		if (v == null){
    			HashSet<Map.Entry<Integer,Integer>> set = new HashSet<>();
    			set.add(new AbstractMap.SimpleEntry<Integer, Integer>(pageEntry));
    			return set;
    		}
    		v.add(new AbstractMap.SimpleEntry<Integer,Integer>(pageEntry));
    		return v;
    	});
    }

    /**
     * Remove the specified tuple from the buffer pool.
     * Will acquire a write lock on the page the tuple is removed from. May block if
     * the lock cannot be acquired.
     *
     * Marks any pages that were dirtied by the operation as dirty.
     *
     * @param tid the transaction adding the tuple.
     * @param tableId the ID of the table that contains the tuple to be deleted
     * @param t the tuple to add
     */
    public synchronized void deleteTuple(int tid, int tableId, Tuple t)
        throws Exception {
    	int pid = t.getPid();
    	Map.Entry<Integer, Integer> key = new AbstractMap.SimpleEntry<>(tableId, pid);
    	HeapPage hp = this.pageCache.get(key);
    	if (hp == null){
    		return; //page is not cached
    	}
    	if (this.holdsWriteLock(tid, tableId, pid)){
    		hp.deleteTuple(t);
    		hp.dirty = true;
    		
    		/*Associate dirty page with transaction*/
        	Map.Entry<Integer, Integer> pageEntry = new AbstractMap.SimpleEntry<>(tableId, pid);
        	this.transToDirtyPageMap.compute(tid, (k , v) -> {
        		if (v == null){
        			HashSet<Map.Entry<Integer,Integer>> set = new HashSet<>();
        			set.add(new AbstractMap.SimpleEntry<Integer, Integer>(pageEntry));
        			return set;
        		}
        		v.add(new AbstractMap.SimpleEntry<Integer,Integer>(pageEntry));
        		return v;
        	});
    	}
    }

    private synchronized  void flushPage(int tableId, int pid) throws IOException {
        Map.Entry<Integer, Integer> key = new AbstractMap.SimpleEntry<>(tableId, pid);
    	HeapPage hp = this.pageCache.get(key);
        Database.getCatalog().writeHeapPage(tableId, hp);
    	hp.dirty = false;
    }

    /**
     * Discards a page from the buffer pool.
     * Flushes the page to disk to ensure dirty pages are updated on disk.
     * 
     * Don't evict a page being locked
     * If all pages are dirty throw an error
     */
    private synchronized  void evictPage() throws Exception {
        HeapPage hp = null;
        Map.Entry<Integer, Integer> k = null;
        for (Map.Entry<Map.Entry<Integer,Integer>, HeapPage> entry : this.pageCache.entrySet()){
        	Map.Entry<Integer, Integer> key = entry.getKey();
        	int tableId = key.getKey();
        	int pid = key.getValue();
        	HeapPage page = entry.getValue();
        	if (page.dirty == false && Database.getCatalog().pageIsLocked(tableId, pid) == false){
        		hp = page;
        		k = key;
        		break;
        	}
        }
        
        if (hp == null){
        	throw new Exception();
        }
        
        /*Remove the page from the cache*/
        this.pageCache.remove(k);
        
    }

}
