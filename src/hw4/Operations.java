package hw4;

import hw1.Tuple;

public class Operations {
	private OperationType type;
	private Tuple t;
	
	public Operations(OperationType type) {
		this.type = type;
		this.t= null;
	}
	public Operations(OperationType type, Tuple t) {
		this.type = type;
		this.t= t;
	}
	public OperationType getType() {
		return this.type;
	}
	public Tuple getTuple() {
		return this.t;
	}
	public String toString() {
		StringBuffer sb= new StringBuffer();
		sb.append("The Operation is : ");
		switch(this.getType()) {
		case READ:
			sb.append("Read");
			break;
		case INSERT:
			sb.append("Insert, inserting (");
			sb.append(t.toString());
			sb.append(")");
			break;
		case DELETE:
			sb.append("Delete, deleting (");
			sb.append(t.toString());
			sb.append(")");
			break;
		default:
			break;
		}
		return sb.toString();
	}
}
