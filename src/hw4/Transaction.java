package hw4;

import java.util.*;

/* this is a new class for transactions*/

public class Transaction {
	private int tid; 
	private List<Map.Entry<Map.Entry<Integer, Integer>, Operations>> tableIdAndPidToOps;
	
	public Transaction(int tid) {
		this.tid = tid;
		this.tableIdAndPidToOps = new LinkedList<>();
	}
	public void addOps(int tableId, int pid, Operations op) {
		Map.Entry<Integer, Integer> newPairTablePage = new AbstractMap.SimpleEntry<>(tableId, pid);
		Map.Entry<Map.Entry<Integer, Integer>, Operations> newEntry = new AbstractMap.SimpleEntry<>(newPairTablePage, op);
		tableIdAndPidToOps.add(newEntry);
	}
	public List<Map.Entry<Map.Entry<Integer, Integer>, Operations>> getTableIdAndPidToOps() {
		return this.tableIdAndPidToOps;
	}
	public Map.Entry<Map.Entry<Integer, Integer>, Operations> getCertainOperation(int index){
		return this.tableIdAndPidToOps.get(index);
	}
	public int getTid() {
		return this.tid;
	}
}
