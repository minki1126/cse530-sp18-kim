package hw2;

import java.util.ArrayList;
import java.util.Iterator;

import hw1.Field;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;
import hw1.RelationalOperator;

/**
 * This class provides methods to perform relational algebra operations. It will be used
 * to implement SQL queries.
 * @author Doug Shook
 * 
 * Daniel Zahka
 *
 */
public class Relation {

	private ArrayList<Tuple> tuples;
	private TupleDesc td;
	
	public Relation(ArrayList<Tuple> l, TupleDesc td) {
		this.tuples = l;
		this.td = td;
	}
	
	/**
	 * This method performs a select operation on a relation
	 * @param field number (refer to TupleDesc) of the field to be compared, left side of comparison
	 * @param op the comparison operator
	 * @param operand a constant to be compared against the given column
	 * @return
	 */
	public Relation select(int field, RelationalOperator op, Field operand) {
		/*Handle field out of bounds*/
		if (field < 0 || field >= td.numFields()){
			return null;
		}

		ArrayList<Tuple> list = new ArrayList<>(this.tuples);
		list.removeIf(t -> !t.getField(field).compare(op, operand));
		return new Relation(list, this.td);
	}
	
	/**
	 * This method performs a rename operation on a relation
	 * @param fields the field numbers (refer to TupleDesc) of the fields to be renamed
	 * @param names a list of new names. The order of these names is the same as the order of field numbers in the field list
	 * @return
	 */
	public Relation rename(ArrayList<Integer> fields, ArrayList<String> names) {
		if (fields.size() != names.size()){return null;}
		/*Handle field out of bounds*/
		for (Integer i : fields){
			if (i < 0 || i >= this.td.numFields()){
				return null;
			}
		}
		Type [] tArr = this.td.getTypeArr();
		String [] fnames = this.td.getFieldNameArr();
		for (int i = 0 ; i < fields.size(); ++i){
			if (fields.get(i) >= 0 && fields.get(i) < this.td.numFields()){
				fnames[fields.get(i)] = names.get(i);
			}
		}
		TupleDesc updatedTd = new TupleDesc(tArr, fnames);
		ArrayList<Tuple> list = new ArrayList<>(this.tuples);
		list.forEach(t -> t.setDesc(updatedTd));
		return new Relation(list, updatedTd);
	}
	
	/**
	 * This method performs a project operation on a relation
	 * @param fields a list of field numbers (refer to TupleDesc) that should be in the result
	 * @return
	 */
	public Relation project(ArrayList<Integer> fields) {
		int length = fields.size();
		Type [] tArr = new Type[length];
		String [] fnames = new String[length];
		for (int i = 0; i < length; ++i){
			tArr[i] = this.td.getType(fields.get(i));
			fnames[i] = this.td.getFieldName(fields.get(i));
		}
		ArrayList<Tuple> projection = new ArrayList<>();
		this.tuples.forEach(t -> {
			Tuple temp = new Tuple(new TupleDesc(tArr, fnames));
			for (int i = 0; i < length; ++i){
				temp.setField(i, t.getField(fields.get(i)));
			}
			projection.add(temp);
	
		});
		return new Relation(projection, new TupleDesc(tArr, fnames));
	}
	
	/**
	 * This method performs a join between this relation and a second relation.
	 * The resulting relation will contain all of the columns from both of the given relations,
	 * joined using the equality operator (=)
	 * @param other the relation to be joined
	 * @param field1 the field number (refer to TupleDesc) from this relation to be used in the join condition
	 * @param field2 the field number (refer to TupleDesc) from other to be used in the join condition
	 * @return
	 */
	public Relation join(Relation other, int field1, int field2) {
		Type[] leftTypes = this.td.getTypeArr();
		String[] leftFieldNames = this.td.getFieldNameArr();
		Type[] rightTypes = other.getDesc().getTypeArr();
		String[] rightFieldNames = other.getDesc().getFieldNameArr();
		Type[] joinedTypes = new Type[leftTypes.length + rightTypes.length];
		String[] joinedFieldNames = new String[leftFieldNames.length + rightFieldNames.length];
		
		System.arraycopy(leftTypes, 0, joinedTypes, 0, leftTypes.length);
		System.arraycopy(rightTypes, 0, joinedTypes, leftTypes.length, rightTypes.length);
		System.arraycopy(leftFieldNames, 0, joinedFieldNames, 0, leftFieldNames.length);
		System.arraycopy(rightFieldNames, 0, joinedFieldNames, leftFieldNames.length, rightFieldNames.length);
		
		TupleDesc joinedTd = new TupleDesc(joinedTypes, joinedFieldNames);
		ArrayList<Tuple> list = new ArrayList<>();
		
		this.tuples.forEach(t ->{
			other.getTuples().forEach(ot -> {
				if (t.getField(field1).equals(ot.getField(field2))){
					Tuple tup = new Tuple(joinedTd);
					for (int i = 0; i < leftTypes.length; ++i){
						tup.setField(i, t.getField(i));
					}
					for (int i = 0; i < rightTypes.length; ++i){
						tup.setField(i + leftTypes.length, ot.getField(i));
					}
					list.add(tup);
				}
			});
		});
		
		/*This is supposed to deal with the duplicate column join case*/
		if (joinedFieldNames[field1].equals(joinedFieldNames[leftFieldNames.length + field2])){
			System.out.println("removing duplicate column");
			ArrayList<Integer> l = new ArrayList<>();
			for (int i = 0; i < joinedFieldNames.length; ++i) {
				if (i != leftFieldNames.length + field2){
					l.add(i);
				}
			}
			return new Relation(list, joinedTd).project(l);
		}
	
		return new Relation(list, joinedTd);
	}
	
	/**
	 * Performs an aggregation operation on a relation. See the lab write up for details.
	 * @param op the aggregation operation to be performed
	 * @param groupBy whether or not a grouping should be performed
	 * @return
	 */
	public Relation aggregate(AggregateOperator op, boolean groupBy) {
		Aggregator a = new Aggregator(op, groupBy, this.getDesc());
		this.getTuples().forEach(t -> a.merge(t));
		return new Relation(a.getResults(), this.td);
	}
	
	public TupleDesc getDesc() {
		return this.td;
	}
	
	public ArrayList<Tuple> getTuples() {
		return this.tuples;
	}
	
	/**
	 * Returns a string representation of this relation. The string representation should
	 * first contain the TupleDesc, followed by each of the tuples in this relation
	 */
	public String toString() {
		//your code here
		return null;
	}
}
