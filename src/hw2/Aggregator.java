package hw2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import hw1.IntField;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;
import hw1.RelationalOperator;
import hw1.StringField;

/**
 * A class to perform various aggregations, by accepting one tuple at a time
 * @author Doug Shook
 *
 */
public class Aggregator {

	private AggregateOperator o;
	private boolean groupBy;
	private TupleDesc td;
	private ArrayList<Tuple> groupTuples;
	private ArrayList<Integer> groupCounts;
	private boolean intField;
	
	
	
	public Aggregator(AggregateOperator o, boolean groupBy, TupleDesc td) {
		this.o = o;
		this.groupBy = groupBy;
		this.td = td;
		this.groupTuples = new ArrayList<>();
		this.groupCounts = new ArrayList<>();
		if (groupBy){
			intField = td.getType(1) == Type.INT ? true : false;
		}
		else{
			intField = td.getType(0) == Type.INT ? true : false;
		}
		//for strings max means later in alphabetical order
	}

	/**
	 * Merges the given tuple into the current aggregation
	 * @param t the tuple to be aggregated
	 */
	public void merge(Tuple t) {
		if (groupBy){
			//see if any of the tuples match
			//if they match merge in
			//otherwise create a new group
			//still need to keep track of the number added
			int group = -1;
			for (int i = 0; i < this.groupTuples.size(); ++i){
				Tuple tt = this.groupTuples.get(i);
				if (tt.getField(0).compare(RelationalOperator.EQ, t.getField(0))){
					group = i;
				}
			}
			if (group == -1){
				group = this.groupTuples.size();
				this.groupTuples.add(t);
				this.groupCounts.add(0);
			}
			if (intField){
				this.intFieldMerge(t, this.groupTuples.get(group), this.groupCounts.get(group), 1);
			}
			else{
				this.stringFieldMerge(t, this.groupTuples.get(group), this.groupCounts.get(group), 1);
			}
			this.groupCounts.set(group, this.groupCounts.get(group) + 1);
		}
		else{
			//still need to make sure group 0 isnt empty (first tuple added)
			if (this.groupCounts.isEmpty()){
				this.groupTuples.add(t);
				this.groupCounts.add(0);
			}
			if (intField){
				this.intFieldMerge(t, this.groupTuples.get(0), this.groupCounts.get(0), 0);
			}
			else{
				this.stringFieldMerge(t, this.groupTuples.get(0), this.groupCounts.get(0), 0);
			}
			this.groupCounts.set(0, this.groupCounts.get(0) + 1);
		}
	}
	
	private void intFieldMerge(Tuple t, Tuple group_t, int groupSize, int fieldNum){
		int v1 = ((IntField) t.getField(fieldNum)).getValue();
		int v2 = ((IntField) group_t.getField(fieldNum)).getValue();
		switch(this.o){
		case MAX:
			group_t.setField(fieldNum, new IntField(Math.max(v1, v2)));
			break;
		case MIN:
			group_t.setField(fieldNum, new IntField(Math.min(v1, v2)));
			break;
		case AVG:
			if (groupSize > 0){
				group_t.setField(fieldNum, new IntField(Math.addExact(v1, v2)));
			}
			break;
		case COUNT:
			group_t.setField(fieldNum, new IntField(groupSize + 1));
			break;
		case SUM:
			if (groupSize > 0){
				group_t.setField(fieldNum, new IntField(Math.addExact(v1, v2)));
			}
			break;
		}
	}
	
	private void stringFieldMerge(Tuple t, Tuple group_t, int groupSize, int fieldNum){
		StringField v1 = (StringField) t.getField(fieldNum);
		StringField v2 = ((StringField) group_t.getField(fieldNum));
		StringField max;
		StringField min;
		if (v1.compare(RelationalOperator.GT, v2)) {
			max = v1;
			min = v2;
		}
		else {
			max = v2;
			min = v1;
		}
		switch(this.o){
		case MAX:
			group_t.setField(fieldNum, max);
			break;
		case MIN:
			group_t.setField(fieldNum, min);
			break;
		case AVG:
			//Don't Think averaging Strings Makes any sense...
			break;
		case COUNT:
			group_t.setField(fieldNum, new IntField(groupSize + 1));
			break;
		case SUM:
			//Don't Think summing strings makes sense...
			break;
		}
	}
	
	/**
	 * Returns the result of the aggregation
	 * @return a list containing the tuples after aggregation
	 */
	public ArrayList<Tuple> getResults() {
		if ( this.o == AggregateOperator.AVG){
			for ( int i = 0; i < this.groupCounts.size(); ++i){
				Tuple t = this.groupTuples.get(i);
				int temp = ((IntField)t.getField(this.groupBy ? 1 : 0)).getValue();
				t.setField(this.groupBy ? 1 : 0, new IntField(temp / this.groupCounts.get(i)));
			}
		}
		return this.groupTuples;
	}

}
