package hw2;

/*Daniel Zahka*/

import hw1.Catalog;
import hw1.Tuple;
import hw1.TupleDesc;
import hw1.Type;
import hw1.Database;
import hw1.Field;
import hw1.RelationalOperator;

import java.util.ArrayList;
import java.util.List;

import net.sf.jsqlparser.JSQLParserException;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.ExpressionVisitor;
import net.sf.jsqlparser.expression.operators.relational.EqualsTo;
import net.sf.jsqlparser.parser.*;
import net.sf.jsqlparser.schema.Column;
import net.sf.jsqlparser.schema.Table;
import net.sf.jsqlparser.statement.*;
import net.sf.jsqlparser.statement.select.FromItem;
import net.sf.jsqlparser.statement.select.Join;
import net.sf.jsqlparser.statement.select.PlainSelect;
import net.sf.jsqlparser.statement.select.Select;
import net.sf.jsqlparser.statement.select.SelectBody;
import net.sf.jsqlparser.statement.select.SelectItem;
import net.sf.jsqlparser.util.TablesNamesFinder;

public class Query {

	private String q;
	
	public Query(String q) {
		this.q = q;
	}
	
	public Relation execute()  {
		Statement statement = null;
		try {
			statement = CCJSqlParserUtil.parse(q);
		} catch (JSQLParserException e) {
			System.out.println("Unable to parse query");
			e.printStackTrace();
		}
		Select selectStatement = (Select) statement;
		PlainSelect sb = (PlainSelect)selectStatement.getSelectBody();
		
		Catalog c = Database.getCatalog();
		
		FromItem from = sb.getFromItem();
		int fromTableId = c.getTableId(from.toString());
		ArrayList<Tuple> tups = c.getDbFile(fromTableId).getAllTuples();
		TupleDesc td = c.getTupleDesc(fromTableId);
		
		/*Make our starting Relation ship from the table in FROM clause*/
		Relation r = new Relation(tups, td);
		
		List<Join> joins = sb.getJoins();
		int joinsLength = joins != null ? joins.size() : 0;
		
		/*Do these joins left to right*/
		for (int i = 0; i < joinsLength; ++ i) {
			Join j = joins.get(i);
			int otherTableId = c.getTableId(j.getRightItem().toString());
			ArrayList<Tuple> othertups = c.getDbFile(otherTableId).getAllTuples();
			TupleDesc td2 = c.getTupleDesc(otherTableId);
			Relation r2 = new Relation(othertups, td2);
			
			Expression joinExpression = j.getOnExpression();
			String jExpression = joinExpression.toString();
			int ii = jExpression.indexOf('.');
			int jj = jExpression.indexOf(' ');
			String fname1 = jExpression.substring(ii + 1, jj);
			ii = jExpression.lastIndexOf('.');
			String fname2 = jExpression.substring(ii + 1);
			
			int f1 = r.getDesc().nameToId(fname1);
			int f2 = r2.getDesc().nameToId(fname2);
			
			r = r.join(r2, f1, f2);
		}
		
		/*Execute WHERE Filter before Projection*/
		Expression where = sb.getWhere();
		if (where != null) {
			WhereExpressionVisitor wvistitor = new WhereExpressionVisitor();
			where.accept(wvistitor);
			String fname = wvistitor.getLeft();
			int fnum = r.getDesc().nameToId(fname);
			Field operand = wvistitor.getRight();
			RelationalOperator op = wvistitor.getOp();
		
			r = r.select(fnum, op, operand);
		}
		
		/*Project onto the columns between SELECT ... FROM*/
		List<SelectItem> l = sb.getSelectItems();
		
		boolean aggregate = false;
		boolean groupBy = q.contains("GROUP BY");
		boolean allColumns = false;
		
		List<String> columnNames = new ArrayList<String>();
		int lLen = l != null ? l.size() : 0;
		for (int k = 0; k < lLen; ++k) {
			SelectItem col = l.get(k);
			ColumnVisitor cv = new ColumnVisitor();
			col.accept(cv);
			aggregate = cv.isAggregate();
			columnNames.add(cv.getColumn());
		}
		
		allColumns = columnNames.get(0).equals("*") ? true : false;
		
		if (aggregate) {
			ColumnVisitor cv = new ColumnVisitor();
			if (groupBy) {
				ArrayList<Integer> proj = new ArrayList<>();
				proj.add(r.getDesc().nameToId(columnNames.get(0)));
				SelectItem rcol = l.get(1);
				rcol.accept(cv);
				proj.add(r.getDesc().nameToId(columnNames.get(1)));
				AggregateOperator op = cv.getOp(); 
				r = r.project(proj);
				r = r.aggregate(op, true);
			}
			else {
				SelectItem col = l.get(0);
				col.accept(cv);
				int colnum = r.getDesc().nameToId(cv.getColumn());
				ArrayList<Integer> proj = new ArrayList<>();
				proj.add(colnum);
				r = r.project(proj);
				AggregateOperator op = cv.getOp(); 
				r = r.aggregate(op, false);
			}
		}
		else if (allColumns) {
			//Do nothing i.e. Project All Columns
		}
		else {
			ArrayList<Integer> plist = new ArrayList<Integer>();
			TupleDesc tdc = r.getDesc();
			columnNames.forEach(name -> plist.add(tdc.nameToId(name)));
			r = r.project(plist);
		} 
		
		return r;
		
	}
}
