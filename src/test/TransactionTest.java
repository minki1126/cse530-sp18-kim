package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.concurrent.*;
import java.nio.file.StandardCopyOption;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import hw4.BufferPool;
import hw4.OperationType;
import hw4.Operations;
import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.HeapPage;
import hw1.IntField;
import hw1.StringField;
import hw4.Permissions;
import hw1.Tuple;
import hw1.TupleDesc;
import hw4.Transaction;

public class TransactionTest {
	
	private Catalog c;
	private BufferPool bp;
	private HeapFile hf;
	private TupleDesc td;
	private int tid;
	//executor service lets parallel programming
	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		int tableId = c.getTableId("test");
		td = c.getTupleDesc(tableId);
		hf = c.getDbFile(tableId);
		//testing for two concurrent transactions, so only need to work with executor that handles two concurrent tasks
		bp = Database.getBufferPool();
		tid = c.getTableId("test");
	}

	@Test
	public void testCommit() throws Exception {
		//regular test, but commit using transaction class
		Tuple t = new Tuple(td);
		t.setField(0, new IntField(new byte[] {0, 0, 0, (byte)131}));
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 98;
		s[2] = 121;
		t.setField(1, new StringField(s));
		Transaction t0 = new Transaction(0);
		Operations insert = new Operations(OperationType.INSERT, t);
		t0.addOps(tid, 0, insert);
		bp.addAndProcessTransaction(t0);
		bp.transactionComplete(0, true); //should flush the modified page
		
		//reset the buffer pool, get the page again, make sure data is there
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage hp = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		Iterator<Tuple> it = hp.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
	}
	@Test
	public void testCommitTwoThreads() throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		//using two transactions and doing them concurrently
		Tuple t1 = new Tuple(td);
		t1.setField(0, new IntField(new byte[] {0, 0, 0, (byte)131}));
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 98;
		s[2] = 121;
		t1.setField(1, new StringField(s));
		Tuple t2 = new Tuple(td);
		t2.setField(0, new IntField(new byte[] {0, 0, 0, (byte)121}));
		t2.setField(1, new StringField(s));
		Transaction transaction0 = new Transaction(0);
		Transaction transaction1 = new Transaction(1);
		Operations insertT1 = new Operations(OperationType.INSERT, t1);
		Operations insertT2 = new Operations(OperationType.INSERT, t2);
		transaction0.addOps(tid, 0, insertT1);
		transaction1.addOps(tid, 0, insertT2);
		//as soon as executor.submit is called, the work should be done by an idle processor
		Future f = executor.submit(()->{
			bp.addAndProcessTransaction(transaction0);
			try {
				bp.transactionComplete(transaction0.getTid(), true);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //should flush the modified page
		});
		//transaction 1 is running on main thread
		bp.addAndProcessTransaction(transaction1);
		bp.transactionComplete(transaction1.getTid(), true);
		//makes sure executor finishes work 
		f.get();
		//reset the buffer pool, get the page again, make sure data is there
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage hp = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		Iterator<Tuple> it = hp.iterator();
		//if both inserts from transaction0 and transaction1 were successful and committed, then there should be 3 tuples in the heap page 
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
	}
	
	@Test
	public void testAbort() throws Exception {
		Tuple t = new Tuple(td);
		t.setField(0, new IntField(new byte[] {0, 0, 0, (byte)131}));
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 98;
		s[2] = 121;
		t.setField(1, new StringField(s));
		Transaction t0 = new Transaction(0);
		Operations insert = new Operations(OperationType.INSERT, t);
		t0.addOps(tid, 0, insert);
		bp.addAndProcessTransaction(t0);
		bp.transactionComplete(0, false); //should abort, discard changes
		
		//reset the buffer pool, get the page again, make sure data is there
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage hp = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		Iterator<Tuple> it = hp.iterator();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
	}
	@Test
	public void testAbortTwoThreads() throws Exception {
		ExecutorService executor = Executors.newFixedThreadPool(2);
		//using two transactions and doing them concurrently
		Tuple t1 = new Tuple(td);
		t1.setField(0, new IntField(new byte[] {0, 0, 0, (byte)131}));
		byte[] s = new byte[129];
		s[0] = 2;
		s[1] = 98;
		s[2] = 121;
		t1.setField(1, new StringField(s));
		Tuple t2 = new Tuple(td);
		t2.setField(0, new IntField(new byte[] {0, 0, 0, (byte)121}));
		t2.setField(1, new StringField(s));
		Transaction transaction0 = new Transaction(0);
		Transaction transaction1 = new Transaction(1);
		Operations insertT1 = new Operations(OperationType.INSERT, t1);
		Operations insertT2 = new Operations(OperationType.INSERT, t2);
		transaction0.addOps(tid, 0, insertT1);
		transaction1.addOps(tid, 0, insertT2);
		//as soon as executor.submit is called, the work should be done by an idle processor
		//since transaction is aborted, insert should not have been done
		Future f = executor.submit(()->{
			bp.addAndProcessTransaction(transaction0);
			try {
				bp.transactionComplete(transaction0.getTid(), false);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} //should flush the modified page
		});
		//transaction 1 is running on main thread
		bp.addAndProcessTransaction(transaction1);
		bp.transactionComplete(transaction1.getTid(), true);
		//makes sure executor finishes work 
		f.get();
		//reset the buffer pool, get the page again, make sure data is there
		Database.resetBufferPool(BufferPool.DEFAULT_PAGES);
		HeapPage hp = bp.getPage(1, tid, 0, Permissions.READ_ONLY);
		Iterator<Tuple> it = hp.iterator();
		//if transaction 0 was successfully aborted while transaction 1 was committed, there should be 2 tuples in the heappage 
		assertTrue(it.hasNext());
		it.next();
		assertTrue(it.hasNext());
		it.next();
		assertFalse(it.hasNext());
	}
}
