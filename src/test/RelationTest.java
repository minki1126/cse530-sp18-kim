package test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import hw2.AggregateOperator;
import hw1.Catalog;
import hw1.Database;
import hw1.HeapFile;
import hw1.IntField;
import hw2.Relation;
import hw1.RelationalOperator;
import hw1.StringField;
import hw1.Tuple;
import hw1.TupleDesc;

public class RelationTest {

	private HeapFile testhf;
	private TupleDesc testtd;
	private HeapFile ahf;
	private TupleDesc atd;
	private Catalog c;

	@Before
	public void setup() {
		
		try {
			Files.copy(new File("testfiles/test.dat.bak").toPath(), new File("testfiles/test.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
			Files.copy(new File("testfiles/A.dat.bak").toPath(), new File("testfiles/A.dat").toPath(), StandardCopyOption.REPLACE_EXISTING);
		} catch (IOException e) {
			System.out.println("unable to copy files");
			e.printStackTrace();
		}
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/test.txt");
		
		int tableId = c.getTableId("test");
		testtd = c.getTupleDesc(tableId);
		testhf = c.getDbFile(tableId);
		
		c = Database.getCatalog();
		c.loadSchema("testfiles/A.txt");
		
		tableId = c.getTableId("A");
		atd = c.getTupleDesc(tableId);
		ahf = c.getDbFile(tableId);
	}
	
	@Test
	public void testSelect() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		ar = ar.select(0, RelationalOperator.EQ, new IntField(530));
		
		
		assert(ar.getTuples().size() == 5);
		assert(ar.getDesc().equals(atd));
	}
	
	@Test
	public void testProject() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(1);
		ar = ar.project(c);
		assert(ar.getDesc().getSize() == 4);
		assert(ar.getTuples().size() == 8);
		assert(ar.getDesc().getFieldName(0).equals("a2"));
	}
	
	@Test
	public void testJoin() {
		Relation tr = new Relation(testhf.getAllTuples(), testtd);
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		tr = tr.join(ar, 0, 0);

		assert(tr.getTuples().size() == 5);
		assert(tr.getDesc().getSize() == 141);
	}
	
	@Test
	public void testRename() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		
		ArrayList<Integer> f = new ArrayList<Integer>();
		ArrayList<String> n = new ArrayList<String>();
		
		f.add(0);
		n.add("b1");
		
		ar = ar.rename(f, n);
		
		assertTrue(ar.getTuples().size() == 8);
		assertTrue(ar.getDesc().getFieldName(0).equals("b1"));
		assertTrue(ar.getDesc().getFieldName(1).equals("a2"));
		assertTrue(ar.getDesc().getSize() == 8);
		
	}
	
	@Test
	public void testAggregate() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(1);
		ar = ar.project(c);
		ar = ar.aggregate(AggregateOperator.SUM, false);
		
		assertTrue(ar.getTuples().size() == 1);
		IntField agg = (IntField) ar.getTuples().get(0).getField(0);
		assertTrue(agg.getValue() == 36);
	}
	@Test
	public void testAvg() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		int n = ar.getTuples().size();
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(1);
		ar = ar.project(c);
		ar = ar.aggregate(AggregateOperator.AVG, false);
		
		assertTrue(ar.getTuples().size() == 1);
		IntField agg = (IntField) ar.getTuples().get(0).getField(0);
		System.out.println(agg.getValue() + " " + 36/n );
		assertTrue(agg.getValue() == 36 / n);
	}
	
	@Test
	public void testGroupBy() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		ar = ar.aggregate(AggregateOperator.SUM, true);
		
		assertTrue(ar.getTuples().size() == 4);
		
		int[] sum = {0};
		ar.getTuples().forEach(t -> sum[0] += ((IntField) t.getField(1)).getValue());
		assertTrue(sum[0] == 36);
	}
	
	@Test
	public void testIntMax() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(0);
		int max = -100000;
		ar = ar.project(c);
		ArrayList<Tuple> tups = ar.getTuples();
		for (int j = 0; j < tups.size(); ++ j){
			int v = ((IntField)tups.get(j).getField(0)).getValue();
			max = max > v ? max : v;
		}
		ar = ar.aggregate(AggregateOperator.MAX, false);
		
		assertTrue(ar.getTuples().size() == 1);
		IntField agg = (IntField) ar.getTuples().get(0).getField(0);
		assertTrue(agg.getValue() == max);
	}
	
	@Test
	public void testIntMin() {
		Relation ar = new Relation(ahf.getAllTuples(), atd);
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(0);
		int min = 100000;
		ar = ar.project(c);
		ArrayList<Tuple> tups = ar.getTuples();
		for (int j = 0; j < tups.size(); ++ j){
			int v = ((IntField)tups.get(j).getField(0)).getValue();
			min = min < v ? min : v;
		}
		ar = ar.aggregate(AggregateOperator.MIN, false);
		
		assertTrue(ar.getTuples().size() == 1);
		IntField agg = (IntField) ar.getTuples().get(0).getField(0);
		assertTrue(agg.getValue() == min);
	}
	
	@Test
	public void testStringMax() {
		Relation ar = new Relation(testhf.getAllTuples(), testtd);
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(1);
		String min = "a";
		ar = ar.project(c);
		ArrayList<Tuple> tups = ar.getTuples();
		for (int j = 0; j < tups.size(); ++ j){
			String v = ((StringField)tups.get(j).getField(0)).getValue();
			min = min.compareTo(v) > 0 ? min : v;
		}
		ar = ar.aggregate(AggregateOperator.MAX, false);
		
		assertTrue(ar.getTuples().size() == 1);
		StringField agg = (StringField) ar.getTuples().get(0).getField(0);
		assertTrue(agg.getValue().equals(min));
	}
	
	@Test
	public void testStringMin() {
		Relation ar = new Relation(testhf.getAllTuples(), testtd);
		ArrayList<Integer> c = new ArrayList<Integer>();
		c.add(1);
		String min = "zzzzzzzzzzzz";
		ar = ar.project(c);
		ArrayList<Tuple> tups = ar.getTuples();
		for (int j = 0; j < tups.size(); ++ j){
			String v = ((StringField)tups.get(j).getField(0)).getValue();
			System.out.println(v);
			min = min.compareTo(v) < 0 ? min : v;
		}
		ar = ar.aggregate(AggregateOperator.MIN, false);
		
		assertTrue(ar.getTuples().size() == 1);
		StringField agg = (StringField) ar.getTuples().get(0).getField(0);
		assertTrue(agg.getValue().equals(min));
	}
}
