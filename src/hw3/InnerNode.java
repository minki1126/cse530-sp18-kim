package hw3;

import java.util.ArrayList;
import java.util.LinkedList;

import hw1.Field;
import hw1.RelationalOperator;

public class InnerNode implements Node {
	
	private int degree;
	private ArrayList<Field> keys;
	private ArrayList<Node> childNodes;
	
	public InnerNode(int degree) {
		this.degree = degree;
		this.keys = new ArrayList<>(degree);
		this.childNodes = new ArrayList<>(degree + 2);
	}
	
	public InnerNode(int degree, ArrayList<Field> keys, ArrayList<Node> children) {
		this.degree = degree;
		this.keys = keys;
		this.childNodes = children;
	}
	
	public ArrayList<Field> getKeys() {
		return this.keys;
	}
	
	public ArrayList<Node> getChildren() {
		return this.childNodes;
	}

	public int getDegree() {
		return this.degree;
	}
	
	public boolean isLeafNode() {
		return false;
	}

	public Node subTreeOf(Field f){
		for (int i = 0; i < this.keys.size(); ++i){
			Field other = this.keys.get(i);
			if (f.compare(RelationalOperator.LTE, other)){
				return childNodes.get(i);
			}
		}
		return childNodes.get(this.keys.size());
	}

	@Override
	public boolean isFull() {
		return this.keys.size() >= this.getDegree();
	}
	
	public void insertKey(Field f, Node newLeftChild, Node newRightChild){
		/*1) Insert the key in correct slot
		 *2) Update the pointers to children*/
		
		/*Find the correct key slot...*/
		int pos = -1;
		for (int i = 0; i < this.keys.size(); ++i){
			Field other = this.keys.get(i);
			if (f.compare(RelationalOperator.LT, other)){
				pos = i;
				break;
			}
		}
		pos = pos == -1 ? this.keys.size() : pos;
		
		/*1)*/
		this.keys.add(pos, f);
		
		/*2)*/
		this.childNodes.add(pos, newLeftChild);
		if (pos + 1 >= this.childNodes.size()){
			this.childNodes.add(newRightChild);
		}
		else{
			this.childNodes.set(pos + 1, newRightChild);
		}
		assert this.isOrdered();
	}
	
	@Override
	public boolean isOrdered() {
		for (int i = 0; i < this.keys.size() - 1; ++i){
			Field l = this.keys.get(i);
			Field r = this.keys.get(i + 1);
			if (!l.compare(RelationalOperator.LT, r)){
				return false;
			}
		}
		return true;
	}

	private int minKeyCapacity(){
		return (int) (Math.ceil((this.degree + 1) / 2.0) - 1);
	}
	
	/*
	 * Minimum capacity is ceil( p / 2 ), so to be able to donate
	 * we need an extra node e.g. ceil ( p / 2 ) + 1
	 * */
	@Override
	public boolean canGive() {
		return this.getKeys().size() >= this.minKeyCapacity() + 1;
	}

	/*
	 * If the node is the leftmost node: return null
	 * */
	@Override
	public Node leftSibling(Node parent) {
		if (parent == null){return null;}
		InnerNode p = (InnerNode) parent;
		for (int i = 0; i < p.getChildren().size(); ++i){
			Node n = p.getChildren().get(i);
			if (this == n){
				//return the left if it exists
				return i == 0 ? null : p.getChildren().get(i - 1);
			}
		}
		return null;
	}

	/*
	 * If the node is the rightmost node: return null
	 * */
	@Override
	public Node rightSibling(Node parent) {
		if (parent == null){return null;}
		InnerNode p = (InnerNode) parent;
		for (int i = 0; i < p.getChildren().size(); ++i){
			Node n = p.getChildren().get(i);
			if (this == n){
				//return the left if it exists
				return i >= p.getChildren().size() - 1 ? null : p.getChildren().get(i + 1);
			}
		}
		return null;
	}
	
	public int indexOfKey(Field f){
		for (int i = 0; i < this.keys.size(); ++i){
			Field other = this.keys.get(i);
			if (f.compare(RelationalOperator.EQ, other)){
				return i;
			}
		}
		return -1;
	}
	
	public void replaceKey(Field oldKey, Field newKey, LinkedList<Node> ancestors){
		if (oldKey == null || newKey == null){return;}
		LinkedList<Node> copyAncestors = (LinkedList<Node>) ancestors.clone(); //for side effect protection
		for (int i = 0; i < this.keys.size(); ++i){
			Field f = this.keys.get(i);
			if (f.compare(RelationalOperator.EQ, oldKey)){
				//Swap with new search key
				this.keys.set(i, newKey);
				return;
			}
		}
		if (copyAncestors != null && copyAncestors.peek() != null && copyAncestors.isEmpty() == false){
			/*Try the parent for the search key*/
			InnerNode parent = (InnerNode) copyAncestors.pop();
			parent.replaceKey(oldKey, newKey, copyAncestors);
		}
	}
	
	public void printSelf(){
		for (int i = 0; i < this.keys.size(); ++i){
			System.out.print(this.keys.get(i).toString() + ", ");
		}
	}
	
	public boolean isUnderflowed(){
		return ! (this.keys.size() >= this.minKeyCapacity());
	}
	public int indexOfSwapKey(Field key){
		for (int i = 0; i < this.keys.size(); ++i){
			Field other = this.keys.get(i);
			if (key.compare(RelationalOperator.LTE, other)){
				return i;
			}
		}
		return -1;
	}
}