package hw3;

import java.util.ArrayList;

import hw1.Field;
import hw1.RelationalOperator;

public class LeafNode implements Node {
	
	private int degree;
	private ArrayList<Entry> entries;
	
	public LeafNode(int degree) {
		this.degree = degree;
		this.entries = new ArrayList<>(degree);
	}
	
	public LeafNode(int degree, ArrayList<Entry> entries){
		this.degree = degree;
		this.entries = entries;
	}
	
	public ArrayList<Entry> getEntries() {
		return this.entries;
	}

	public int getDegree() {
		return this.degree;
	}
	
	public boolean isLeafNode() {
		return true;
	}
	
	public boolean containsField(Field f){
		for (Entry e: this.getEntries()){
			Field lf = e.getField();
			if (lf.compare(RelationalOperator.EQ, f)){
				//The key is in this Leaf
				return true;
			}
		}
		//The key cannot be in this leaf
		return false;
	}
	
	public void insertEntry(Entry e){
		Field other = e.getField();
		for (int i = 0; i < this.entries.size(); ++i){
			Field f = this.entries.get(i).getField();
			if (other.compare(RelationalOperator.LT, f)){
				this.entries.add(i, e);
				
				assert this.isOrdered();
				
				return;
			}
		}
		this.entries.add(e);
		
		assert this.isOrdered();
	}

	@Override
	public boolean isFull() {
		return this.entries.size() >= this.getDegree();
	}

	@Override
	public boolean isOrdered() {
		for (int i = 0; i < this.entries.size() - 1; ++i){
			Field l = this.entries.get(i).getField();
			Field r = this.entries.get(i + 1).getField();
			if (!l.compare(RelationalOperator.LT, r)){
				return false;
			}
		}
		return true;
	}

	/*
	 * Minimum capacity is ceil( p / 2 ), so to be able to donate
	 * we need an extra node e.g. ceil ( p / 2 ) + 1
	 * */
	@Override
	public boolean canGive() {
		return this.getEntries().size() >= Math.ceil(this.degree / 2.0) + 1;
	}

	/*
	 * If the node is the leftmost node: return null
	 * */
	@Override
	public Node leftSibling(Node parent) {
		InnerNode p = (InnerNode) parent;
		for (int i = 0; i < p.getChildren().size(); ++i){
			Node n = p.getChildren().get(i);
			if (this == n){
				//return the left if it exists
				return i == 0 ? null : p.getChildren().get(i - 1);
			}
		}
		return null;
	}

	/*
	 * If the node is the rightmost node: return null
	 * */
	@Override
	public Node rightSibling(Node parent) {
		InnerNode p = (InnerNode) parent;
		for (int i = 0; i < p.getChildren().size(); ++i){
			Node n = p.getChildren().get(i);
			if (this == n){
				//return the left if it exists
				return i >= p.getChildren().size() - 1 ? null : p.getChildren().get(i + 1);
			}
		}
		return null;
	}

	/*
	 * Delete entry from leaf node and return left entry
	 * Assuming degree > 2 and this method is being called there should always 
	 * be a left node.
	 * 
	 * */
	public Entry deleteEntry(Entry e){
		Entry left = null;
		for (int i = 0; i < this.entries.size(); ++i){
			Entry other = this.entries.get(i);
			if (e.equals(other)){
				left = i >= 1 ? this.entries.get(i - 1) : null;
				this.entries.remove(i);
				return left;
			}
		}
		return null;
	}
	public void printSelf(){
		for (int i = 0; i < this.entries.size(); ++i){
			System.out.print(this.entries.get(i).getField().toString() + ", ");
		}
	}
}