package hw3;

public interface Node {
	
	
	public int getDegree();
	public boolean isLeafNode();
	public boolean isFull();
	public boolean isOrdered();
	public boolean canGive();
	public Node leftSibling(Node parent);
	public Node rightSibling(Node parent);
	public void printSelf();
	
}
