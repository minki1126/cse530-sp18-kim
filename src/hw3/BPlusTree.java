package hw3;


import hw1.Field;
import hw1.RelationalOperator;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class BPlusTree {
    
	private Node root;
	private int degree;
	
    public BPlusTree(int degree) {
    	this.degree = degree;
    	root = new LeafNode(degree);
    }
    
    public LeafNode search(Field f) {
    	LeafNode leaf = this.searchKernel(root, f);
    	return leaf.containsField(f) ? leaf : null;
    }
    
    private LeafNode searchKernel(Node n, Field f){
    	if (n.isLeafNode()){
    		return (LeafNode) n;
    	}
    	
    	InnerNode innerNode = (InnerNode) n;
    	Node subtree = innerNode.subTreeOf(f);
    	return this.searchKernel(subtree, f);
    }
    
    private LinkedList<Node> getLeafLineage(Field f){
    	LinkedList<Node> stack = new LinkedList<>();
    	return this.getLeafLineageKernel(this.root, stack, f);
    }
    
    private LinkedList<Node> getLeafLineageKernel(Node n, LinkedList<Node> stack, Field f){
    	stack.push(n);
    	if (n.isLeafNode()){
    		return stack;
    	}
    	
    	InnerNode innerNode = (InnerNode) n;
    	Node subtree = innerNode.subTreeOf(f);
    	return this.getLeafLineageKernel(subtree, stack, f);
    }
    
    public void insert(Entry e) {
    	Field f = e.getField();
    	LinkedList<Node> lineage = this.getLeafLineage(f);
    	
    	assert lineage.peek().isLeafNode();
    	
    	LeafNode leaf = (LeafNode)lineage.pop();
    	
    	/*Entry Already There, do Noting*/
    	if (leaf.containsField(f)){
    		return;
    	}
    	
    	this.insertKernel(leaf, lineage, e, null, null);
    	
    }
    
    private void insertKernel(Node n, LinkedList<Node> ancestors, Entry e, Node newLeftChild, Node newRightChild){
    	Field f = e.getField();
    	if (!n.isFull()){
    		//Trivial, no split needed
    		if (n.isLeafNode()){
    			/*No pointers to adjust, just insert in right spot*/
    			LeafNode leaf = (LeafNode) n;
    			leaf.insertEntry(e);
    		}
    		else{
    			/*Make sure pointers still make sense*/
    			InnerNode innerNode = (InnerNode) n;
    			innerNode.insertKey(f, newLeftChild, newRightChild);
    		}
    		return;
    	}
    	
    	/*Need to split and recursively insert key into parent*/
    	Node parent;
    	Node newLeft;
    	Node newRight;
    	Entry newEntry;
    	if (n == this.root){
    		parent = new InnerNode(this.degree);
    		this.root = parent;
    	}
    	else{
    		parent = ancestors.pop();
    	}
    	
    	/*parent should be valid inner Node at this point*/
    	int median = (this.degree + 1) / 2;
    	if (n.isLeafNode()){
    		median = this.degree % 2 != 0 ? median - 1 : median;
    		
    		/*We keep the median in the leaf after we pass*/
    		LeafNode leaf = (LeafNode) n;
    		
    		leaf.insertEntry(e);
    		ArrayList<Entry> entries = leaf.getEntries();
    		
    		Field medianKey = entries.get(median).getField();
    		
    		List<Entry> leftEntries = entries.subList(0, median + 1);
    		List<Entry> rightEntries = entries.subList(median + 1, entries.size());
    		
    		newEntry = new Entry(medianKey, 0); 
    		
    		newLeft = new LeafNode(this.degree, new ArrayList<Entry>(leftEntries));
    		newRight = new LeafNode(this.degree, new ArrayList<Entry>(rightEntries));
    		
    	}
    	else{
    		/*We get rid of the median when we pass*/
    		InnerNode innerNode = (InnerNode) n;
    		
    		innerNode.insertKey(f, newLeftChild, newRightChild);
    		ArrayList<Field> keys = innerNode.getKeys();
    		ArrayList<Node> children = innerNode.getChildren();
    		
    		Field medianKey = keys.get(median);
    		
    		List<Field> leftKeys = keys.subList(0, median);
    		List<Field> rightKeys = keys.subList(median + 1, keys.size());
    		
    		List<Node> leftChildren = children.subList(0, median + 1);
    		List<Node> rightChildren = children.subList(median + 1, children.size());
    		
    		newEntry = new Entry(medianKey, 0);
    		
    		newLeft = new InnerNode(this.degree, new ArrayList<Field>(leftKeys), new ArrayList<Node>(leftChildren));
    		newRight = new InnerNode(this.degree, new ArrayList<Field>(rightKeys), new ArrayList<Node>(rightChildren));

    	}
    	
    	this.insertKernel(parent, ancestors, newEntry, newLeft, newRight);
    }    
     
   /* private static ArrayList<Field> stripFields(List<Entry> list){
    	ArrayList<Field> ret = new ArrayList<>();
    	for(int i = 0; i < list.size(); ++i){
    		Field f = list.get(i).getField();
    		ret.add(f);
    	}
		return ret;
    }
    */
    
    public void delete(Entry e) {
    	Field f = e.getField();
    	LinkedList<Node> lineage = this.getLeafLineage(f);
    	
    	assert lineage.peek().isLeafNode();
    	
    	LeafNode leaf = (LeafNode)lineage.pop();
    	
    	/*Entry is not there, do Noting*/
    	if (leaf.containsField(f) == false){
    		return;
    	}
    	
    	/*Delete the entry with field*/
    	this.deleteFromLeaf(e, leaf, lineage);
    }
    
    public void deleteFromLeaf(Entry e, LeafNode n, LinkedList<Node> ancestors){
    	Field f = e.getField();
    	
    	if (n == this.root){
    		n.deleteEntry(e);
    		return; /*done*/
    	}
    	
    	if (n.canGive()){
    		/*trivial case: delete entry and try to update parent keys*/
    		Entry temp = n.deleteEntry(e);
    		Field newKey = temp == null ? null : temp.getField();
    		/*Try to update parent if applicable*/
    		InnerNode parent = (InnerNode) ancestors.pop();
    		parent.replaceKey(f, newKey, ancestors);
    		return; /*done*/
    	}
    	
    	/*Next let's try to steal from siblings in order:
    	 * 1) Left
    	 * 2) right
    	 * */
    	InnerNode parent = (InnerNode) ancestors.pop();
    	LeafNode left = (LeafNode) n.leftSibling(parent);
    	LeafNode right = (LeafNode) n.rightSibling(parent);
    	
    	/*1)*/
    	if (left != null && left.canGive()){
    		int leftSize = left.getEntries().size();
    		/*get the rightmost entry in left sibling*/
    		Entry stolen = left.getEntries().get(leftSize - 1);
    		/*insert our stolen entry*/
    		n.insertEntry(stolen);
    		Entry temp = left.deleteEntry(stolen);
    		Field newLeftKey = temp == null ? null : temp.getField();
    		parent.replaceKey(stolen.getField(), newLeftKey, ancestors);
    		
    		/*actually delete e*/
    		temp = n.deleteEntry(e);
    		Field maybeNewKey = temp == null ? null : temp.getField();
    		parent.replaceKey(e.getField(), maybeNewKey, ancestors);
    		return; /*done*/
    	}
    	
    	/*2)*/
    	else if (right != null && right.canGive()){
    		/*always take leftmost -> index 0*/
    		Entry stolen = right.getEntries().get(0);
    		int ourSize = n.getEntries().size();
    		/*Stale key*/
    		Field oldKey = n.getEntries().get(ourSize - 1).getField();
    		/*insert our stolen entry*/
    		n.insertEntry(stolen);
    		right.deleteEntry(stolen);
    		parent.replaceKey(oldKey, stolen.getField(), ancestors);
    		
    		/*Actually delete e*/
    		n.deleteEntry(e);
    		return; /*done*/
    	}
    	
    	/*Need to do sibling merging and recursively delete a key from inner node*/
    	if (left != null){
    		Field oldKey = left.getEntries().get(left.getEntries().size() - 1).getField();
    		Field parentDeleteKey = n.getEntries().get(n.getEntries().size() -1).getField();
    		/*give all our entries to left*/
    		for (Entry entry : n.getEntries()){
    			left.insertEntry(entry);
    		}
    		
    		left.deleteEntry(e);
    		Field newKey = left.getEntries().get(left.getEntries().size() - 1).getField();
    		
    		//Delete the right most key in n...
    		this.deleteInner(parentDeleteKey, parent, ancestors);
    		parent.replaceKey(oldKey, newKey, ancestors);
    	}
    	else{
    		Field oldKey = n.getEntries().get(n.getEntries().size() - 1).getField();
    		Field parentDeleteKey = n.getEntries().get(n.getEntries().size() -1).getField();
    		/*Take all entries from right*/
    		for (Entry entry : right.getEntries()){
    			n.insertEntry(entry);
    		}
    		
    		n.deleteEntry(e);
    		Field newKey = n.getEntries().get(n.getEntries().size() - 1).getField();
    		//Delete the right most key in n...
    		this.deleteInner(parentDeleteKey, parent, ancestors);
    		parent.replaceKey(oldKey, newKey, ancestors);
    	}
    	
    }
    
    /**/
    public void deleteInner(Field key, InnerNode n, LinkedList<Node> ancestors){
    	if (n == this.root && n.getKeys().size() <= 1){
    		System.out.println("HI");
    		/*special edge case here... just put leaves back together*/
    		LeafNode l = (LeafNode) n.getChildren().get(0);
    		this.root = l;
    		return;
    	}
    	
    	InnerNode parent = ancestors.isEmpty() == false ? (InnerNode) ancestors.pop() : null;
    	//if (n.canGive()){
    		/*Trivial case*/
    		int keyPos = - 1;
    		for (int i = 0; i < n.getKeys().size(); ++i){
    			Field currentKey = n.getKeys().get(i);
    			if (key.compare(RelationalOperator.EQ, currentKey)){
    				keyPos = i;
    			}
    		}
    		int last = n.getKeys().size() - 1;
    		Field oldKey = n.getKeys().remove(keyPos == -1 ? last : keyPos);
    		if (keyPos == -1){
    			n.getChildren().remove(n.getChildren().size() - 1);
    			InnerNode p = parent;
    			if (p != null){
    				p.replaceKey(key, oldKey, ancestors);
    			}
    		}else{
    			n.getChildren().remove(keyPos);
    		}
    		
    	//}
    	if (n.isUnderflowed() == false || n == this.root){return;}
    	/*Next let's try to steal from siblings in order:
    	 * 1) Left
    	 * 2) right
    	 * */
    	//InnerNode parent = (InnerNode) ancestors.pop();
    	InnerNode left = (InnerNode) n.leftSibling(parent);
    	InnerNode right = (InnerNode) n.rightSibling(parent);
    	
    	if (left != null && left.canGive()){
    		Field keyToRotateUp = left.getKeys().get(left.getKeys().size() - 1);
    		int idx = parent.indexOfSwapKey(keyToRotateUp);
    		Field keyToRotateDown = parent.getKeys().get(idx);
    		parent.getKeys().set(idx, keyToRotateUp);
    		Node childToSteal = left.getChildren().get(left.getChildren().size() -1);
    		
    		/*Tack them on to us...*/
    		//n.getKeys().add(0, keyToSteal);
    		n.getChildren().add(0, childToSteal);
    		
    		/*Delete them from the left now*/
    		left.getKeys().remove(left.getKeys().size() - 1);
    		left.getChildren().remove(left.getChildren().size() - 1); 
    		
    		//int removeIdx = n.indexOfKey(key);
    		//n.getKeys().remove(removeIdx);
    		n.getKeys().add(0, keyToRotateDown);
    		
    		return; /*done*/
    	}
    	else if (right != null && right.canGive()){
    		Field nodeRightKey = n.getKeys().get(n.getKeys().size() - 1); //right most key
    		Field keyToRotateUp = right.getKeys().get(0);
    		int idx = parent.indexOfSwapKey(nodeRightKey);
    		Field keyToRotateDown = parent.getKeys().get(idx);
    		parent.getKeys().set(idx, keyToRotateUp); //Rotate up
    		Node childToSteal = right.getChildren().get(0);
    		
    		/*Tack them on to us...*/
    		n.getKeys().add(keyToRotateDown);
    		n.getChildren().add(childToSteal);
    		
    		/*Delete them from the right now*/
    		right.getKeys().remove(0);
    		right.getChildren().remove(0);
    		
    		return; /*done*/
    	}
    	
    	/*Now we need to deal with the pulling down from parent
    	 * as well as the root edge case...
    	 * 
    	 * */
    	if (parent == this.root && parent.getKeys().size() == 1){
    		/*Need to do a merge...*/
    		this.root = left;
    		left.getKeys().add(parent.getKeys().get(0));
    		left.getKeys().addAll(n.getKeys());
    		left.getChildren().addAll(n.getChildren());
    		return;
    	}
    	/*Pull down technique and some possible recursion...*/
    	if (left != null){
    		int idx = parent.indexOfSwapKey(left.getKeys().get(left.getKeys().size() - 1));
    		Field keyToDel = parent.getKeys().get(idx);
    		n.getKeys().add(0, keyToDel);
    		
    		left.getKeys().addAll(n.getKeys());
    		left.getChildren().addAll(n.getChildren());
    		
    		parent.getChildren().set(idx + 1, left); //change the ptr
    		
    		/*recur*/
    		this.deleteInner(keyToDel, parent, ancestors);
    		return;
    	}
    	if (right != null){
    		int idx = parent.indexOfSwapKey(n.getKeys().get(n.getKeys().size() - 1));
    		Field keyToDel = parent.getKeys().get(idx);
    		n.getKeys().add(0, keyToDel);
    		
    		n.getKeys().addAll(right.getKeys());
    		n.getChildren().addAll(right.getChildren());
    		
    		/*recur*/
    		this.deleteInner(keyToDel, parent, ancestors);
    		return;
    	}
    	
    }
    public Node getRoot() {
    	return this.root;
    }
    
    public void printTree(){
    	ArrayList<ArrayList<Node>> levels = new ArrayList<ArrayList<Node>>();
    	int depth = 1;
    	ArrayList<Node> level0 = new ArrayList<>();
    	level0.add(this.root);
    	levels.add(level0);
    	this.printTreeKernel(this.root, levels, depth);
    	int pad = levels.get(levels.size() - 1).size() / 2 + 1;
    	for (ArrayList<Node> level : levels){
    		for (int j = 0; j < pad; ++j){
    			System.out.print("       ");
    		}
    		pad = (pad + 1) / 2;
    		for (Node node : level){
    			node.printSelf();
    			System.out.print("       ");
    		}
    		System.out.println();
    	}
    }
    public void printTreeKernel(Node n, ArrayList<ArrayList<Node>> levels, int depth){
    	if (n.isLeafNode()){
    		return;
    	}
    	if (depth >= levels.size()){
    		levels.add(new ArrayList<Node>());
    	}
    	InnerNode inode = (InnerNode) n;
    	/*Add all children*/
    	levels.get(depth).addAll(inode.getChildren());
    	for (Node child : inode.getChildren()){
    		this.printTreeKernel(child, levels, depth + 1);
    	}
    }

	
}
